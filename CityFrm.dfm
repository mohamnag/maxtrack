object CityF: TCityF
  Left = 253
  Top = 236
  BorderStyle = bsToolWindow
  Caption = 'Cities'
  ClientHeight = 353
  ClientWidth = 208
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TntBevel10: TTntBevel
    Left = 0
    Top = 0
    Width = 9
    Height = 353
    Align = alLeft
    Shape = bsSpacer
  end
  object TntBevel5: TTntBevel
    Left = 200
    Top = 0
    Width = 8
    Height = 353
    Align = alRight
    Shape = bsSpacer
  end
  object Panel1: TPanel
    Left = 9
    Top = 0
    Width = 191
    Height = 353
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object TntBevel2: TTntBevel
      Left = 0
      Top = 0
      Width = 191
      Height = 7
      Align = alTop
      Shape = bsSpacer
    end
    object TntLabel17: TTntLabel
      Left = 0
      Top = 7
      Width = 191
      Height = 13
      Align = alTop
      Caption = 'Select your location from cities:'
    end
    object TntBevel4: TTntBevel
      Left = 0
      Top = 20
      Width = 191
      Height = 7
      Align = alTop
      Shape = bsSpacer
    end
    object TntShape2: TTntShape
      Left = 0
      Top = 27
      Width = 191
      Height = 53
      Align = alTop
      Brush.Color = clInfoBk
      Pen.Color = clGray
      Pen.Style = psDot
      Shape = stRoundRect
    end
    object TntLabel18: TTntLabel
      Left = 7
      Top = 34
      Width = 184
      Height = 39
      AutoSize = False
      Caption = 
        'To set your current location, select contry and the city then. P' +
        'ress Set after selecting.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMenuHighlight
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      WordWrap = True
    end
    object TntBevel1: TTntBevel
      Left = 0
      Top = 80
      Width = 191
      Height = 7
      Align = alTop
      Shape = bsSpacer
    end
    object TntBevel3: TTntBevel
      Left = 0
      Top = 204
      Width = 191
      Height = 7
      Align = alBottom
      Shape = bsSpacer
    end
    object TntLabel1: TTntLabel
      Left = 0
      Top = 87
      Width = 191
      Height = 13
      Align = alTop
      Caption = 'Country list:'
    end
    object TntLabel2: TTntLabel
      Left = 0
      Top = 211
      Width = 191
      Height = 13
      Align = alBottom
      Caption = 'City list:'
    end
    object TntBevel6: TTntBevel
      Left = 0
      Top = 346
      Width = 191
      Height = 7
      Align = alBottom
      Shape = bsSpacer
    end
    object TntBevel7: TTntBevel
      Left = 0
      Top = 295
      Width = 191
      Height = 7
      Align = alBottom
      Shape = bsSpacer
    end
    object Panel2: TPanel
      Left = 0
      Top = 302
      Width = 191
      Height = 44
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object TntShape5: TTntShape
        Left = 0
        Top = 0
        Width = 145
        Height = 44
        Brush.Color = clCream
        Pen.Color = clGray
        Pen.Style = psDot
        Shape = stRoundRect
      end
      object TntLabel47: TTntLabel
        Left = 24
        Top = 8
        Width = 18
        Height = 13
        Caption = 'Lon'
        Transparent = True
      end
      object LblLon: TTntLabel
        Left = 48
        Top = 8
        Width = 15
        Height = 13
        Caption = '0.0'
        Transparent = True
      end
      object TntLabel48: TTntLabel
        Left = 27
        Top = 24
        Width = 15
        Height = 13
        Caption = 'Lat'
        Transparent = True
      end
      object LblLat: TTntLabel
        Left = 48
        Top = 24
        Width = 15
        Height = 13
        Caption = '0.0'
        Transparent = True
      end
      object TntSpeedButton2: TTntSpeedButton
        Left = 151
        Top = 0
        Width = 40
        Height = 22
        Caption = 'Set'
        Flat = True
        OnClick = TntSpeedButton2Click
      end
      object TntSpeedButton1: TTntSpeedButton
        Left = 151
        Top = 22
        Width = 40
        Height = 22
        Caption = 'Exit'
        Flat = True
        OnClick = TntSpeedButton1Click
      end
    end
    object CountryLst: TTntListBox
      Left = 0
      Top = 100
      Width = 191
      Height = 104
      Align = alClient
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      ItemHeight = 13
      TabOrder = 1
      OnClick = CountryLstClick
    end
    object CityLst: TTntListBox
      Left = 0
      Top = 224
      Width = 191
      Height = 71
      Align = alBottom
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      ItemHeight = 13
      TabOrder = 2
      OnClick = CityLstClick
    end
  end
end
