unit CityFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TntExtCtrls, StdCtrls, TntStdCtrls, Buttons,
  TntButtons;

type
  TCityF = class(TForm)
    Panel1: TPanel;
    TntBevel2: TTntBevel;
    TntLabel17: TTntLabel;
    TntBevel4: TTntBevel;
    TntShape2: TTntShape;
    TntLabel18: TTntLabel;
    TntBevel1: TTntBevel;
    Panel2: TPanel;
    TntShape5: TTntShape;
    TntLabel47: TTntLabel;
    LblLon: TTntLabel;
    TntLabel48: TTntLabel;
    LblLat: TTntLabel;
    TntSpeedButton2: TTntSpeedButton;
    TntBevel3: TTntBevel;
    TntLabel1: TTntLabel;
    TntLabel2: TTntLabel;
    TntBevel10: TTntBevel;
    TntBevel5: TTntBevel;
    TntBevel6: TTntBevel;
    TntBevel7: TTntBevel;
    TntSpeedButton1: TTntSpeedButton;
    CountryLst: TTntListBox;
    CityLst: TTntListBox;
    procedure FormShow(Sender: TObject);
    procedure CountryLstClick(Sender: TObject);
    procedure CityLstClick(Sender: TObject);
    procedure TntSpeedButton1Click(Sender: TObject);
    procedure TntSpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
type
  CityDat = record
    Name : string;
    Lon : currency;
    Lat : currency;
  end;

  CountryDat = record
    Name : string;
    Cities : array of CityDat;
    CityCount : word;
  end;

  GeoDat = record
    CountryCount : word;
    CountryData : array of CountryDat;
  end;



var
  CityF: TCityF;
  GeoData : GeoDat;
implementation

{$R *.dfm}

procedure TCityF.FormShow(Sender: TObject);
var
TmpS : tstrings;
i,j: word;
str : string;
begin
TmpS := tstringlist.Create;
TmpS.LoadFromFile(extractfiledir(application.ExeName)+'\Cities.dat');
LblLon.Caption := '0.0';
LblLat.Caption := '0.0';
CountryLst.Items.Clear;
CityLst.Items.Clear;
GeoData.CountryCount := 0;

if TmpS.Count <> 0 then
  for i := 0 to TmpS.Count-1 do begin
    if TmpS[i] <> '' then
      if TmpS[i][1] = '@' then begin
        CountryLst.Items.Add(copy(TmpS[i], 2, length(TmpS[i])-1));
        SetLength(GeoData.CountryData, GeoData.CountryCount + 1);
        GeoData.CountryData[GeoData.CountryCount].Name := CountryLst.Items[GeoData.CountryCount];
        GeoData.CountryData[GeoData.CountryCount].CityCount := 0;
        for j := i+1 to TmpS.Count-1 do begin
          if (TmpS[j] = '') then break;
          if (TmpS[j][1] <> '>') then break;
          str := TmpS[j];
          Setlength(GeoData.CountryData[GeoData.CountryCount].Cities, GeoData.CountryData[GeoData.CountryCount].CityCount + 1);
          GeoData.CountryData[GeoData.CountryCount].Cities[GeoData.CountryData[GeoData.CountryCount].CityCount].Name := copy(str, 2, pos(',', str)-2);
          delete(str, 1, pos(',', str));
          GeoData.CountryData[GeoData.CountryCount].Cities[GeoData.CountryData[GeoData.CountryCount].CityCount].Lon := StrToCurr(Copy(str, 1, pos(',', str)-1));
          delete(str, 1, pos(',', str));
          GeoData.CountryData[GeoData.CountryCount].Cities[GeoData.CountryData[GeoData.CountryCount].CityCount].Lat := StrToCurr(Copy(str, 1, length(str)));
          GeoData.CountryData[GeoData.CountryCount].CityCount := GeoData.CountryData[GeoData.CountryCount].CityCount + 1;
        end;
        GeoData.CountryCount := GeoData.CountryCount + 1;
      end;
    end;

end;

procedure TCityF.CountryLstClick(Sender: TObject);
var
i: word;
begin

CityLst.Items.Clear;
if CountryLst.ItemIndex <> -1 then begin
  for i := 0 to GeoData.CountryData[CountryLst.ItemIndex].CityCount-1 do
    CityLst.Items.Add(GeoData.CountryData[CountryLst.ItemIndex].Cities[i].Name);
end;
end;

procedure TCityF.CityLstClick(Sender: TObject);
var
i: word;
begin
  if CityLst.ItemIndex <> -1 then begin
    LblLon.Caption := CurrToStr(GeoData.CountryData[CountryLst.ItemIndex].Cities[CityLst.ItemIndex].Lon);
    LblLat.Caption := CurrToStr(GeoData.CountryData[CountryLst.ItemIndex].Cities[CityLst.ItemIndex].Lat);
  end;
end;

procedure TCityF.TntSpeedButton1Click(Sender: TObject);
begin
CityF.ModalResult := mrcancel;
end;

procedure TCityF.TntSpeedButton2Click(Sender: TObject);
begin
if CityLst.ItemIndex = -1 then begin
  ShowMessage('You have not already selected any city!');
  exit;
end;
CityF.ModalResult := mrok;
end;

end.
