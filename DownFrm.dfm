object DownF: TDownF
  Left = 331
  Top = 171
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Update Kepler Data'
  ClientHeight = 219
  ClientWidth = 208
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object TntBevel5: TTntBevel
    Left = 200
    Top = 0
    Width = 8
    Height = 219
    Align = alRight
    Shape = bsSpacer
  end
  object TntBevel10: TTntBevel
    Left = 0
    Top = 0
    Width = 9
    Height = 219
    Align = alLeft
    Shape = bsSpacer
  end
  object Panel1: TPanel
    Left = 9
    Top = 0
    Width = 191
    Height = 219
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object TntShape2: TTntShape
      Left = 0
      Top = 27
      Width = 191
      Height = 53
      Align = alTop
      Brush.Color = clInfoBk
      Pen.Color = clGray
      Pen.Style = psDot
      Shape = stRoundRect
    end
    object TntBevel1: TTntBevel
      Left = 0
      Top = 20
      Width = 191
      Height = 7
      Align = alTop
      Shape = bsSpacer
    end
    object TntLabel18: TTntLabel
      Left = 4
      Top = 33
      Width = 184
      Height = 39
      AutoSize = False
      Caption = 
        'Please wait until update finishs. Update Kepler Data frequently ' +
        'to have the best tracking results.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMenuHighlight
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      WordWrap = True
    end
    object TntBevel4: TTntBevel
      Left = 0
      Top = 0
      Width = 191
      Height = 7
      Align = alTop
      Shape = bsSpacer
    end
    object TntBevel2: TTntBevel
      Left = 0
      Top = 80
      Width = 191
      Height = 17
      Align = alTop
      Shape = bsSpacer
    end
    object TntLabel17: TTntLabel
      Left = 0
      Top = 7
      Width = 191
      Height = 13
      Align = alTop
      Caption = 'Downlading updates from internet:'
    end
    object TntLabel1: TTntLabel
      Left = 0
      Top = 105
      Width = 79
      Height = 13
      Caption = 'Overall progress:'
    end
    object TntLabel3: TTntLabel
      Left = 0
      Top = 160
      Width = 79
      Height = 13
      Caption = 'This file progress'
    end
    object Panel2: TPanel
      Left = 16
      Top = 128
      Width = 153
      Height = 17
      BevelInner = bvLowered
      TabOrder = 0
      object AllProg: TGauge
        Left = 2
        Top = 2
        Width = 149
        Height = 13
        Align = alClient
        BackColor = clBtnFace
        BorderStyle = bsNone
        Color = clGradientActiveCaption
        ForeColor = clBlue
        ParentColor = False
        Progress = 0
      end
    end
    object Panel3: TPanel
      Left = 16
      Top = 183
      Width = 153
      Height = 17
      BevelInner = bvLowered
      TabOrder = 1
      object FileProg: TGauge
        Left = 2
        Top = 2
        Width = 149
        Height = 13
        Align = alClient
        BackColor = clBtnFace
        BorderStyle = bsNone
        Color = clGradientActiveCaption
        ForeColor = clSkyBlue
        ParentColor = False
        Progress = 0
      end
    end
  end
end
