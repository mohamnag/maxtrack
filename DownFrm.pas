unit DownFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TntExtCtrls, StdCtrls, TntStdCtrls, Gauges, ExtActns;

type
  TDownF = class(TForm)
    TntBevel5: TTntBevel;
    TntBevel10: TTntBevel;
    Panel1: TPanel;
    TntBevel1: TTntBevel;
    TntLabel18: TTntLabel;
    TntShape2: TTntShape;
    TntBevel4: TTntBevel;
    TntBevel2: TTntBevel;
    TntLabel17: TTntLabel;
    TntLabel1: TTntLabel;
    Panel2: TPanel;
    AllProg: TGauge;
    TntLabel3: TTntLabel;
    Panel3: TPanel;
    FileProg: TGauge;
  private
    { Private declarations }
    function FileNameOfURL(URL : string):string;
    procedure URL_OnDownloadProgress
        (Sender: TDownLoadURL;
         Progress, ProgressMax: Cardinal;
         StatusCode: TURLDownloadStatus;
         StatusText: String; var Cancel: Boolean);
  public
    { Public declarations }
    procedure DownUpdates(Dir: string);
  end;

var
  DownF: TDownF;

implementation

uses MainFrm, Math;

{$R *.dfm}

{ TDownF }

procedure TDownF.DownUpdates(Dir: string);
var
i,j : word;
err : Boolean;
Downer : TDownLoadURL;
errNo, doneNo : Word;
label
newtry;
begin

  errNo := 0;
  doneNo := 0;
  Downer := TDownLoadURL.Create(self);
  If not DirectoryExists(Dir + '\tmp') then
    MkDir(Dir + '\tmp');

    try
    try
      AllProg.MaxValue := MainF.UpdateDownLst.Items.Count;
      for i := 0 to MainF.UpdateDownLst.Items.Count-1 do begin
        DownF.Repaint;

        Downer.URL := MainF.UpdateDownLst.Items[i];
        Downer.FileName := Dir + '\tmp\' + FileNameOfURL(MainF.UpdateDownLst.Items[i]);
        Downer.OnDownloadProgress := URL_OnDownloadProgress;

        j := 0;

        newtry:
        err := false;
        try
          Downer.ExecuteTarget(nil) ;
        except
          j := j+1;
          err := true;
        end;

        if err then
          if j > 5 then
            ShowMessage('An error occured when trying to download following URL:'+#13+#13+
                  Downer.URL)
          else
            goto newtry;

        AllProg.Progress := i;

        if not err then
        begin

          CopyFile(pchar(Dir + '\tmp\' + FileNameOfURL(MainF.UpdateDownLst.Items[i])),
                  pchar(Dir + '\' + FileNameOfURL(MainF.UpdateDownLst.Items[i])),
                  false);

          DeleteFile(Dir + '\tmp\' + FileNameOfURL(MainF.UpdateDownLst.Items[i]));

          doneNo := doneNo + 1;
        end
        else
          errNo := errNo + 1;

      end;
    except
    end;

    finally
      Downer.free;
      Downer := nil;
      ShowMessage('Updating done with '+IntToStr(doneNo)+' seccessful download(s) and,'+#13+
                  IntToStr(errNo)+' error(s).' );
    end;
end;

function TDownF.FileNameOfURL(URL: string): string;
begin
while pos('/', URL) <> 0 do
  URL := copy(URL, pos('/', URL)+1, length(URL) - pos('/', URL)+1);

Result := URL;
end;

procedure TDownF.URL_OnDownloadProgress(Sender: TDownLoadURL; Progress,
  ProgressMax: Cardinal; StatusCode: TURLDownloadStatus;
  StatusText: String; var Cancel: Boolean);
begin
  FileProg.MaxValue := ProgressMax;
  FileProg.Progress := Progress;
end;

end.
