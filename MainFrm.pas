unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, jpeg, ExtCtrls, StdCtrls, TntExtCtrls, ComCtrls, TntComCtrls,
  TntStdCtrls, TntMenus, CheckLst, TntCheckLst, LMDControl, LMDBaseControl,
  LMDBaseGraphicControl, LMDGraphicControl, LMDClock, Buttons, TntButtons,
  ImgList, registry, LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel,
  LMDSimplePanel, LMDCustomComponent, LMDCustomHint, LMDCustomShapeHint,
  LMDMessageHint, shellapi, DJTSatLib_TLB, ComObj, LMDContainerComponent,
  LMDBaseDialog, LMDDirDlg, ElFolderDlg, math, WinSkinData, CPort;

type
SatPointerRec = record
  Img: TImage;
  Lbl: TLabel;
  Hint: string;
end;

SatsDataRec = record
  Name: string;
  Visible: boolean;
  SouthBound: WordBool;
  az,
  el,
  lon,
  lat,
  range,
  range_rate,
  altitude: double;
  poiner: SatPointerRec;
end;

type
  TMainF = class(TForm)
    TntPageControl1: TTntPageControl;
    TntTabSheet1: TTntTabSheet;
    WorldMap: TImage;
    TntTabSheet2: TTntTabSheet;
    TntLabel1: TTntLabel;
    TntLabel2: TTntLabel;
    TntLabel3: TTntLabel;
    TntLabel4: TTntLabel;
    TntLabel5: TTntLabel;
    TntLabel6: TTntLabel;
    TntLabel7: TTntLabel;
    TntLabel8: TTntLabel;
    TntLabel10: TTntLabel;
    TntLabel11: TTntLabel;
    TntLabel12: TTntLabel;
    TntLabel13: TTntLabel;
    TntLabel14: TTntLabel;
    TntLabel15: TTntLabel;
    TntLabel16: TTntLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    TntBevel7: TTntBevel;
    Panel3: TPanel;
    TntShape6: TTntShape;
    TntLabel58: TTntLabel;
    TntBevel8: TTntBevel;
    TntPanel1: TTntPanel;
    TntShape2: TTntShape;
    TntLabel17: TTntLabel;
    TntLabel18: TTntLabel;
    TntBevel4: TTntBevel;
    TntBevel1: TTntBevel;
    FullSatLst: TTntCheckListBox;
    TntBevel5: TTntBevel;
    Panel4: TPanel;
    TntBevel6: TTntBevel;
    TntLabel19: TTntLabel;
    TntBevel3: TTntBevel;
    TntShape1: TTntShape;
    TntBevel9: TTntBevel;
    TntLabel20: TTntLabel;
    TntLabel21: TTntLabel;
    TrackSatStatLst: TTntComboBox;
    TntPanel2: TTntPanel;
    TntShape3: TTntShape;
    TntLabel25: TTntLabel;
    TntLabel27: TTntLabel;
    TntLabel26: TTntLabel;
    TntLabel24: TTntLabel;
    TntLabel23: TTntLabel;
    TntLabel22: TTntLabel;
    TntLabel31: TTntLabel;
    TntLabel32: TTntLabel;
    LblTrackSatVis: TTntLabel;
    LblTrackSatAz: TTntLabel;
    LblTrackSatEl: TTntLabel;
    LblTrackSatLon: TTntLabel;
    LblTrackSatLat: TTntLabel;
    LblTrackSatDistance: TTntLabel;
    LblTrackSatSpeed: TTntLabel;
    LblTrackSatAltitude: TTntLabel;
    TntBevel14: TTntBevel;
    TntBevel16: TTntBevel;
    TntTabSheet3: TTntTabSheet;
    TntBevel17: TTntBevel;
    Panel6: TPanel;
    TntBevel18: TTntBevel;
    TntBevel19: TTntBevel;
    TntLabel62: TTntLabel;
    TntBevel21: TTntBevel;
    TntShape7: TTntShape;
    TntLabel63: TTntLabel;
    TntBevel22: TTntBevel;
    TntBevel2: TTntBevel;
    TntBevel20: TTntBevel;
    TntLabel64: TTntLabel;
    TntSpeedButton1: TTntSpeedButton;
    Panel7: TPanel;
    TntLabel67: TTntLabel;
    TntBevel23: TTntBevel;
    TntShape8: TTntShape;
    TntLabel68: TTntLabel;
    TntBevel24: TTntBevel;
    TntBevel25: TTntBevel;
    TntLabel70: TTntLabel;
    TntSpeedButton4: TTntSpeedButton;
    KDataPathEdt: TTntEdit;
    TntBevel26: TTntBevel;
    TntBevel27: TTntBevel;
    UpdateURLEdit: TTntEdit;
    TntLabel65: TTntLabel;
    UpdateDownLst: TTntListBox;
    TntSpeedButton5: TTntSpeedButton;
    Panel8: TPanel;
    TntBevel28: TTntBevel;
    TntLabel66: TTntLabel;
    TntBevel29: TTntBevel;
    TntShape9: TTntShape;
    TntLabel72: TTntLabel;
    TntBevel30: TTntBevel;
    TntSpeedButton6: TTntSpeedButton;
    TntTabSheet4: TTntTabSheet;
    TntBevel31: TTntBevel;
    Panel9: TPanel;
    TntLabel73: TTntLabel;
    TntBevel32: TTntBevel;
    TntShape10: TTntShape;
    TntLabel74: TTntLabel;
    TntBevel33: TTntBevel;
    TntBevel34: TTntBevel;
    HDInit: TTntSpeedButton;
    TntPanel4: TTntPanel;
    TntShape11: TTntShape;
    TntLabel85: TTntLabel;
    TntLabel75: TTntLabel;
    TntLabel76: TTntLabel;
    TntLabel77: TTntLabel;
    LblHDDetection: TTntLabel;
    TntLabel79: TTntLabel;
    ComPortsCB: TTntComboBox;
    TntSpeedButton10: TTntSpeedButton;
    TntSpeedButton11: TTntSpeedButton;
    TntShape13: TTntShape;
    TntLabel83: TTntLabel;
    ImageList1: TImageList;
    LblTip: TTntLabel;
    LocPointer: TImage;
    HideLocHint: TTimer;
    LocHint: TLMDMessageHint;
    TntLabel9: TTntLabel;
    Dirlookup: TElFolderDialog;
    TntSpeedButton12: TTntSpeedButton;
    NewTipBtn: TTntSpeedButton;
    EssStep: TTntLabel;
    WorldMapPop: TTntPopupMenu;
    Showonlynowtrackingsat1: TTntMenuItem;
    ShowalltrackedsatsslowesdownPC1: TTntMenuItem;
    N1: TTntMenuItem;
    Selectrefreshrate1: TTntMenuItem;
    N1seccond1: TTntMenuItem;
    N5seccond1: TTntMenuItem;
    N10seccond1: TTntMenuItem;
    N30seccond1: TTntMenuItem;
    N1minute1: TTntMenuItem;
    N5minutes1: TTntMenuItem;
    TrackSatTimer: TTimer;
    VisSatPointer: TImage;
    Panel5: TPanel;
    TntBevel11: TTntBevel;
    TntLabel44: TTntLabel;
    TntBevel12: TTntBevel;
    TntShape4: TTntShape;
    TntBevel13: TTntBevel;
    TntLabel45: TTntLabel;
    TntLabel51: TTntLabel;
    TntLabel57: TTntLabel;
    TntLabel56: TTntLabel;
    TntLabel52: TTntLabel;
    TntLabel53: TTntLabel;
    TntLabel54: TTntLabel;
    TntLabel55: TTntLabel;
    TntSpeedButton2: TTntSpeedButton;
    TntSpeedButton3: TTntSpeedButton;
    TntLabel78: TTntLabel;
    TntLabel84: TTntLabel;
    LonEdt: TTntEdit;
    LatEdt: TTntEdit;
    LocNameEdt: TTntEdit;
    HeightEdt: TTntEdit;
    TntPanel3: TTntPanel;
    TntShape5: TTntShape;
    TntLabel46: TTntLabel;
    TntLabel47: TTntLabel;
    TntLabel48: TTntLabel;
    LblLocLon: TTntLabel;
    LblLocLat: TTntLabel;
    TntLabel59: TTntLabel;
    LblLocHeight: TTntLabel;
    LblLocName: TTntLabel;
    TntLabel61: TTntLabel;
    LblLocMinElv: TTntLabel;
    MinElvEdt: TTntEdit;
    TntBevel10: TTntBevel;
    TntBevel15: TTntBevel;
    Panel10: TPanel;
    TntBevel37: TTntBevel;
    TntLabel80: TTntLabel;
    TntBevel38: TTntBevel;
    TntShape12: TTntShape;
    TntLabel81: TTntLabel;
    TntBevel39: TTntBevel;
    TntLabel82: TTntLabel;
    TntSpeedButton8: TTntSpeedButton;
    TntSpeedButton9: TTntSpeedButton;
    TrackSatOrdLst: TTntListBox;
    TntBevel36: TTntBevel;
    TntBevel35: TTntBevel;
    TntBevel42: TTntBevel;
    TntSpeedButton13: TTntSpeedButton;
    SkinData1: TSkinData;
    LocPointerLbl: TTntLabel;
    TntLabel29: TTntLabel;
    TntLabel30: TTntLabel;
    TntLabel33: TTntLabel;
    TntLabel34: TTntLabel;
    KDDateGroup: TGroupBox;
    KDNewDate: TTntLabel;
    KDNewLbl: TTntLabel;
    KDOldDate: TTntLabel;
    KDOldLbl: TTntLabel;
    AuthenticationCheck: TTntCheckBox;
    TntLabel35: TTntLabel;
    TntBevel40: TTntBevel;
    TntBevel41: TTntBevel;
    UTCDisplay: TTntLabel;
    UTCDisplayTimer: TTimer;
    TntBevel43: TTntBevel;
    UTCDateDisplay: TTntLabel;
    KeplerAlert: TTntCheckBox;
    AlertDays: TTntEdit;
    TntLabel36: TTntLabel;
    TntLabel37: TTntLabel;
    TntLabel38: TTntLabel;
    TntBevel44: TTntBevel;
    TntBevel45: TTntBevel;
    TntLabel28: TTntLabel;
    TntBevel46: TTntBevel;
    NotVisSatPointer: TImage;
    TntBevel47: TTntBevel;
    KeplerAlertTimer: TTimer;
    ComPort1: TComPort;
    HDLog: TTntMemo;
    HDDisConnect: TTntSpeedButton;
    DataTransmit: TTimer;
    procedure TntEdit4KeyPress(Sender: TObject; var Key: Char);
    procedure TntSpeedButton10Click(Sender: TObject);
    procedure TntSpeedButton11Click(Sender: TObject);
    procedure TntSpeedButton3Click(Sender: TObject);
    procedure TntSpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LocPointerMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure HideLocHintTimer(Sender: TObject);
    procedure TntSpeedButton4Click(Sender: TObject);
    procedure TntSpeedButton6Click(Sender: TObject);
    procedure NewTipBtnClick(Sender: TObject);
    procedure FullSatLstClickCheck(Sender: TObject);
    procedure TntSpeedButton9Click(Sender: TObject);
    procedure TntSpeedButton8Click(Sender: TObject);
    procedure VisSatPointerMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TrackSatTimerTimer(Sender: TObject);
    procedure N1seccond1Click(Sender: TObject);
    procedure N5seccond1Click(Sender: TObject);
    procedure N10seccond1Click(Sender: TObject);
    procedure N30seccond1Click(Sender: TObject);
    procedure N1minute1Click(Sender: TObject);
    procedure N5minutes1Click(Sender: TObject);
    procedure TrackSatStatLstChange(Sender: TObject);
    procedure TntSpeedButton13Click(Sender: TObject);
    procedure LocPointerLblMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure SatPointerLblMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TntSpeedButton12Click(Sender: TObject);
    procedure TntSpeedButton1Click(Sender: TObject);
    procedure TntSpeedButton5Click(Sender: TObject);
    procedure UTCDisplayTimerTimer(Sender: TObject);
    procedure KeplerAlertClick(Sender: TObject);
    procedure KeplerAlertKeyPress(Sender: TObject; var Key: Char);
    procedure KeplerAlertTimerTimer(Sender: TObject);
    procedure AlertDaysChange(Sender: TObject);
    procedure HDInitClick(Sender: TObject);
    procedure HDDisConnectClick(Sender: TObject);
    procedure DataTransmitTimer(Sender: TObject);
  private
    { Private declarations }
    procedure FreeSatPointers(SDA : array of SatsDataRec);
    procedure MapDeg();
    procedure ReadLocData();
    procedure ShowSat(out Sat : SatsDataRec; SatName: string);
    procedure ReadTrackLst();
    Procedure TrackSats();
    procedure MakeVisRange(SatName : string);
    procedure MakeVisRange2(SatName : string);
    procedure KDataDate();
    procedure SaveUpdateURLs();
    procedure LoadUpdateURLs();
    procedure DrawFootPrint(RS, slat, slon : Double);

    procedure TransmitData(az, el : Double; turn:string);

    function FNatn(x, y : Double): Double;
    function ChkSum(a, b: char) : Byte;

    function ExecAndWait(const ExecuteFile, ParamString : string): boolean;
    function NowUTC: TDateTime;
    function WriteStrInReg(KeyName, KeyValue: string):boolean;
    function ReadStrFromReg(KeyName: string):string;

  public
    { Public declarations }
    ISat: ISatellites;
    TrHDAZ, TRHDEL : Double;
    TrTurn : String;

  end;

var
  MainF: TMainF;
  SatsDataArray: array of SatsDataRec;
  VisRange : array of TPoint;
  HDInited : Boolean = False;

const
  foot_print_points : Integer = 200;
  HDInitHeader : String = 'MXT';
  HDAZHeader : Char = #25;

implementation

uses AboutFrm, CityFrm, DownFrm, StartUpFrm;

{$R *.dfm}

function IntToBin(value: Byte):string;
var I: byte;
begin
  { Convert from integer to binary }
  for I:=0 to 7 do begin
    Result := IntToStr((Value shr I) mod 2) + Result;
  end;
end;

function BinToInt(Number: string):Byte;
var I,L: Byte;
begin
  { Convert from binary to integer }
  Result := 0;
  L := length(Number);
  for I := 1 to L do begin
    Result := Result + (trunc(power(2,L-I))*strtoint(Number[i]));
  end;
end;

function TMainF.NowUTC: TDateTime;
var
  system_datetime: TSystemTime;
begin
  GetSystemTime (system_datetime);
  Result := SystemTimeToDateTime (system_datetime);
end;


procedure TMainF.MapDeg;
var
i: word;
Deg, Step: real;
begin
if FileExists(ExtractFileDir(ParamStr(0)) + '\' + 'UserMap.bmp') then
  WorldMap.Picture.LoadFromFile(ExtractFileDir(ParamStr(0)) + '\' + 'UserMap.bmp');
WorldMap.Canvas.Pen.Style := psSolid;
WorldMap.Canvas.Pen.Color := clwhite;
WorldMap.Canvas.Pen.Width := 1;
Deg := WorldMap.Width / 12;
Step := Deg;
for i := 0 to 10 do begin
  WorldMap.Canvas.PenPos := Point(round(Deg), 2);
  WorldMap.Canvas.LineTo(round(Deg), WorldMap.Height-12);
  Deg := Deg + Step;
end;

Deg := WorldMap.Height / 6;
Step := Deg;
for i := 0 to 4 do begin
  WorldMap.Canvas.PenPos := Point(24, round(Deg));
  WorldMap.Canvas.LineTo(WorldMap.Width-2, round(Deg));
  Deg := Deg + Step;
end;

WorldMap.Canvas.Pen.Style := psDash;
WorldMap.Canvas.Brush.Style := bsVertical; 
Deg := 5*(WorldMap.Height / 36) + (WorldMap.Height /2);
WorldMap.Canvas.Polyline([Point(24, round(Deg)), Point(WorldMap.Width-2, round(Deg))]);

Deg := (WorldMap.Height /2) - 5*(WorldMap.Height / 36);
WorldMap.Canvas.PenPos := Point(24, round(Deg));
WorldMap.Canvas.LineTo(WorldMap.Width-2, round(Deg));

end;

procedure TMainF.TntEdit4KeyPress(Sender: TObject; var Key: Char);
begin
if not ( ((ord(key) <= 57) and (ord(key) >= 48)) or (key = #8)
        or (key = '.') or ((key = '-') and ((Sender as TTntEdit).Text = '') ))  then begin
  key := chr(0);
  beep;
end;

end;

procedure TMainF.TntSpeedButton10Click(Sender: TObject);
begin
  MainF.Close;

end;

procedure TMainF.TntSpeedButton11Click(Sender: TObject);
begin
AboutF.ShowModal;

end;

procedure TMainF.TntSpeedButton3Click(Sender: TObject);
begin
  if not assigned(CityF) then
    CityF := TCityF.Create(self);
    
  if CityF.ShowModal = mrok then begin
    LonEdt.Text := CityF.LblLon.Caption;
    LatEdt.Text := CityF.LblLat.Caption;
    LocNameEdt.Text := CityF.CityLst.Items[CityF.CityLst.ItemIndex];
  end;
end;

function TMainF.WriteStrInReg(KeyName, KeyValue: string): boolean;
var
Reg: tregistry;
begin

  reg := tregistry.Create;
  result := false;
  try
    reg.RootKey := HKEY_LOCAL_MACHINE;
    reg.OpenKey('\SOFTWARE\IVS\MN\MaxTrack',true);
    reg.WriteString(KeyName, KeyValue);
    result := true;
  finally
    reg.Free;
  end;

end;

procedure TMainF.TntSpeedButton2Click(Sender: TObject);
begin
  WriteStrInReg('LocLon', LonEdt.Text);
  WriteStrInReg('LocLat', LatEdt.Text);
  WriteStrInReg('LocHeight', HeightEdt.Text);
  WriteStrInReg('LocMinElv', MinElvEdt.Text);
  WriteStrInReg('LocName', LocNameEdt.Text);
  ReadLocData;
  NewTipBtn.Click;
end;

function TMainF.ReadStrFromReg(KeyName: string): string;
var
Reg: tregistry;
begin

  reg := tregistry.Create;
  result := '';
  try
    reg.RootKey := HKEY_LOCAL_MACHINE;
    reg.OpenKey('\SOFTWARE\IVS\MN\MaxTrack', false);
    result := reg.ReadString(KeyName);
  finally
    reg.Free;
  end;

end;

procedure TMainF.ReadLocData;
var
str:string;
tmp:currency;
begin
str := ReadStrFromReg('LocName');
if str <> '' then begin
  LblLocName.Caption := str;
  LocNameEdt.Text := LblLocName.Caption;

  LblLocLon.Caption := ReadStrFromReg('LocLon');
  LonEdt.Text := LblLocLon.Caption;
  ISat.ObsLon := StrToFloat(LblLocLon.Caption);

  LblLocLat.Caption := ReadStrFromReg('LocLat');
  LatEdt.Text := LblLocLat.Caption;
  ISat.ObsLat := StrToFloat(LblLocLat.Caption);

  LblLocHeight.Caption := ReadStrFromReg('LocHeight');
  HeightEdt.Text := LblLocHeight.Caption;
  ISat.ObsHeight := StrToFloat(LblLocHeight.Caption);

  LblLocMinElv.Caption := ReadStrFromReg('LocMinElv');
  MinElvEdt.Text := LblLocMinElv.Caption;
//  ISat.

  tmp := StrToCurr(LblLocLon.Caption);
  if tmp > 180 then tmp := tmp - 360;

  LocPointer.Left := Trunc( WorldMap.Width / 2 - LocPointer.Width /2 + (tmp * WorldMap.Width) / 360 );
  LocPointer.Top := WorldMap.Height - Trunc( WorldMap.Height / 2 + LocPointer.Height / 2
                    + ( (StrToCurr(LblLocLat.Caption) * WorldMap.Height) / 180 )-WorldMap.Top);
  LocPointer.Visible := true;

  LocPointerLbl.Caption := LblLocName.Caption;
  if (LocPointer.Left + LocPointerLbl.Width) < WorldMap.Width then
    LocPointerLbl.Left := LocPointer.Left
  else
    LocPointerLbl.Left := LocPointer.Left + LocPointer.Width - LocPointerLbl.Width;

  if (LocPointer.Top + LocPointerLbl.Height) < WorldMap.Height then
    LocPointerLbl.Top := LocPointer.Top + LocPointer.Height
  else
    LocPointerLbl.Top := LocPointer.Top - LocPointerLbl.Height;
  LocPointerLbl.Visible := true;



end;
end;

procedure TMainF.FormCreate(Sender: TObject);
label LoadDll;
begin

  if not Assigned(StartUpF) then
    StartUpF := TStartUpF.Create(self);
  StartUpF.Show;

  StartUpF.LoadProgLbl.Caption := 'Loading DLLs';
  StartUpF.Repaint;
  sleep(800);

 LoadDll:
  try
    // Try and connect to the Active-X DLL
    ISat := CreateComObject (CLASS_Satellites) as Satellites;
  except
  end;

  if not Assigned(ISat) then begin
    if fileexists(extractfiledir(application.ExeName)+'\DJTSatLib.dll') then begin
      if MessageBox(Handle, 'The DJTSatLib.dll is not registered,'+#13+
                  'Max Track will try to register it now.'+#13+#13+
                  'If it''s seccond time you see this message,'+#13+
                  'Try using the command "REGSVR32 DJTSatLib.dll" '+#13+
                  'from the command prompt in the folder containing '+#13+
                  'the file DJTSatLib.dll.', 'Error', MB_OKCANCEL) = mrCancel then
      begin
        Application.Terminate;
        Exit;
      end;
      
      ExecAndWait('REGSVR32', '/s ' + Pchar(extractfiledir(application.ExeName)) + '\DJTSatLib.dll');
      goto LoadDll;
    end
    else begin
      ShowMessage('The DJTSatLib.dll is not registered,'+#13+
                  'As this file dose not exists in root of Max Track'+#13+
                  'program can''t try to register it.'+#13+#13+
                  'Try using the command "REGSVR32 DJTSatLib.dll" '+#13+
                  'from the command prompt in the folder containing '+#13+
                  'the file DJTSatLib.dll.'+#13+#13+
                  'Or copy the file to Max Track''s folder and press OK');
      goto LoadDll;
    end;
  end;

  ReadLocData;

  StartUpF.LoadProgLbl.Caption := 'Loading Kepler Data';
  StartUpF.Repaint;
  sleep(800);
  KDataPathEdt.Text := ReadStrFromReg('KDataPath');
  if (not directoryexists(KDataPathEdt.Text)) and (KDataPathEdt.Text <> '') then begin
    ShowMessage('Later you have set a Kepler Data path,'+#13+
                'whitch now dose not exists.'+#13+
                'Please try setting new directory.');
    KDataPathEdt.Text := '';
  end;
  if KDataPathEdt.Text <> '' then begin
    ISat.KeplerPath := KDataPathEdt.Text;
    FullSatLst.Items.CommaText := ISat.SatNames;
    ReadTrackLst;
    KDataDate;
  end
  else begin
    ShowMessage('The Kepler Data is not set'+#13+
                'so no satellites are defined.'+#13+#13+
                'Before you can do any tracking,'+#13+
                'go to "Kepler Data" tab and set the'+#13+
                'Kepler Data path there.');
  end;

  NewTipBtn.Click;

  StartUpF.LoadProgLbl.Caption := 'Loading tracking satellites list';
  StartUpF.Repaint;
  sleep(800);

  LoadUpdateURLs;
  if ReadStrFromReg('KeplerAlert') = '1' then
  begin
    AlertDays.Text := ReadStrFromReg('KeplerAlertInterval');
    if AlertDays.Text = '' then
      AlertDays.Text := '20';
    KeplerAlertTimer.Enabled := true;
    AlertDays.Enabled := True;
    KeplerAlert.Checked := true;
    KeplerAlertTimerTimer(self);
  end;

  StartUpF.LoadProgLbl.Caption := 'Listing up COM ports';
  StartUpF.Repaint;
  sleep(800);

  EnumComPorts(ComPortsCB.Items.AnsiStrings);
  if ComPortsCB.Items.Count > 0 then
  begin
    HDLog.Lines.Add(IntToStr(ComPortsCB.Items.Count)+' Ports detected');
    ComPortsCB.ItemIndex := 0;
  end
  else
  begin
    HDLog.Lines.Add('No ports detected');
    ComPortsCB.Enabled := false;
    HDInit.Enabled := false;
    ShowMessage('No ports found in this computer,'+#13+
                'so you cann''t use hardware.');
  end;

  MapDeg;

  TrackSats;
  StartUpF.Hide;
  MainF.BringToFront;
end;

procedure TMainF.LocPointerMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
LocHint.ShowControlMessage(
      LblLocName.Caption+#13+#13+
      'Lon: '+LblLocLon.Caption+#13+
      'Lat: '+LblLocLat.Caption
      , (Sender as Tcontrol));
HideLocHint.Enabled := true;
end;

procedure TMainF.HideLocHintTimer(Sender: TObject);
begin
LocHint.HideMessage;
end;

procedure TMainF.TntSpeedButton4Click(Sender: TObject);
begin
  Dirlookup.Parent := MainF;
  Dirlookup.DialogTitle := 'Select Kepler Data Path';
  if Dirlookup.ExecuteModal then begin
    KDataPathEdt.Text := Dirlookup.Folder;
    WriteStrInReg('KDataPath', KDataPathEdt.Text);
    ISat.KeplerPath := KDataPathEdt.Text;
    FullSatLst.Items.CommaText := ISat.SatNames;
  end;
end;

procedure TMainF.TntSpeedButton6Click(Sender: TObject);
begin
  if KDataPathEdt.Text <> '' then begin
    ISat.KeplerPath := KDataPathEdt.Text;
    FullSatLst.Items.CommaText := ISat.SatNames;
    ReadTrackLst;
    KDataDate;
    ShowMessage('Kepler Data reloaded.');
  end;
end;

procedure TMainF.NewTipBtnClick(Sender: TObject);
begin
if LblLocName.Caption = 'Not defined' then begin
LblTip.Caption := 'Go to Hardware tab and set the Land location to start.'+
                  ' Click "New tip" if you have done it!';
EssStep.Caption := 'Step 1 of 4';
end
else if KDataPathEdt.Text = '' then begin
LblTip.Caption := 'Now you have to set the Kepler Data.'+
                  ' Go to this tab and set Directory path in which'+
                  ' you have these Data. You can download them from'+
                  ' Internet';
EssStep.Caption := 'Step 2 of 4';
end
else if TrackSatStatLst.Items.Count = 0 then begin
LblTip.Caption := 'Congratulation! Now you can select the satellites'+
                  ' you want to track. Go to Tracking tab and do it.'+
                  'You can change satellite''s importance order in'+
                  ' Hardware tab.';
EssStep.Caption := 'Step 3 of 4';
end
else if not HDInited then begin
LblTip.Caption := 'It''s last essential step to set program working!'+#13+
                  'Go to Hardware tab and set the Hardware initialization'+
                  ' parameters and initialize it.';
EssStep.Caption := 'Step 4 of 4';
end
else begin
LblTip.Caption := 'OK! you have done every thing nessecery.'+#13+
                  'Now fill free to work with other parts!';
EssStep.Caption := '';

end;

end;

procedure TMainF.FullSatLstClickCheck(Sender: TObject);
var
changed : boolean;
TmpS : WideString;
begin
  changed := false;
  if not FullSatLst.Checked[FullSatLst.ItemIndex] then begin
    TmpS := TrackSatOrdLst.Items.Text;
    TrackSatOrdLst.Items.Text := copy(TrackSatOrdLst.Items.Text, 1, pos(
                                      FullSatLst.Items[FullSatLst.itemIndex],
                                      TrackSatOrdLst.Items.Text)-1);
    delete(TmpS, 1, pos(FullSatLst.Items[FullSatLst.itemIndex],
                        TmpS)+length(FullSatLst.Items[FullSatLst.itemIndex])+1);
    TrackSatOrdLst.Items.Text := TrackSatOrdLst.Items.Text + TmpS;
    changed := true;
  end
  else begin
    if (FullSatLst.Checked[FullSatLst.ItemIndex] and
       (pos(FullSatLst.Items[FullSatLst.ItemIndex], TrackSatOrdLst.Items.Text) = 0))
       then begin
      TrackSatOrdLst.Items.Add(FullSatLst.Items[FullSatLst.ItemIndex]);
      changed := true;
    end;
  end;

  if changed then begin
    TrackSatOrdLst.Items.SaveToFile(ExtractFileDir(Application.ExeName)+'\TrackSats.dat');
    TrackSatStatLst.Items.Text := TrackSatOrdLst.Items.Text;
    TrackSats;
  end;
end;

procedure TMainF.ReadTrackLst;
var
i: integer;
begin
TrackSatStatLst.Items.LoadFromFile(ExtractFileDir(Application.ExeName)+'\TrackSats.dat');
TrackSatOrdLst.Items.LoadFromFile(ExtractFileDir(Application.ExeName)+'\TrackSats.dat');
for i := 0 to FullSatLst.Items.Count - 1 do
  if pos(FullSatLst.Items[i], TrackSatOrdLst.Items.Text) <> 0 then
    FullSatLst.Checked[i] := true;
end;

procedure TMainF.TntSpeedButton9Click(Sender: TObject);
var
TmpS: WideString;
begin
if TrackSatOrdLst.ItemIndex = (TrackSatOrdLst.Items.Count - 1) then exit;
TmpS := TrackSatOrdLst.Items[TrackSatOrdLst.ItemIndex];
TrackSatOrdLst.Items[TrackSatOrdLst.ItemIndex] := TrackSatOrdLst.Items[TrackSatOrdLst.ItemIndex+1];
TrackSatOrdLst.Items[TrackSatOrdLst.ItemIndex+1] := TmpS;
TrackSatOrdLst.ItemIndex := TrackSatOrdLst.ItemIndex + 1;
TrackSatOrdLst.Items.SaveToFile(ExtractFileDir(Application.ExeName)+'\TrackSats.dat');
TrackSatStatLst.Items.Text := TrackSatOrdLst.Items.Text;
TrackSats;
end;

procedure TMainF.TntSpeedButton8Click(Sender: TObject);
var
TmpS: WideString;
begin
if TrackSatOrdLst.ItemIndex = 0 then exit;
TmpS := TrackSatOrdLst.Items[TrackSatOrdLst.ItemIndex];
TrackSatOrdLst.Items[TrackSatOrdLst.ItemIndex] := TrackSatOrdLst.Items[TrackSatOrdLst.ItemIndex-1];
TrackSatOrdLst.Items[TrackSatOrdLst.ItemIndex-1] := TmpS;
TrackSatOrdLst.ItemIndex := TrackSatOrdLst.ItemIndex - 1;
TrackSatOrdLst.Items.SaveToFile(ExtractFileDir(Application.ExeName)+'\TrackSats.dat');
TrackSatStatLst.Items.Text := TrackSatOrdLst.Items.Text;
TrackSats;
end;

procedure TMainF.TrackSats;
var
  i: integer;
  phase: double;
  FirstVis : boolean;
  MinElev, jt: double;
  ME : String;
const
  Rad2Deg = 180 / pi;

begin

  ME := ReadStrFromReg('LocMinElv');

  if (not Assigned(ISat)) or (TrackSatOrdLst.Count = 0) or (ME = '') then Exit;

  FreeSatPointers(SatsDataArray);
  setlength(SatsDataArray, TrackSatOrdLst.Count);
  FirstVis := true;

  MinElev := StrToFloat(ME);
  jt := ISat.DateTimeToJT(nowUTC);

  for i := 0 to TrackSatOrdLst.Count -1 do begin
    SatsDataArray[i].Name := TrackSatOrdLst.Items[i];
    SatsDataArray[i].Visible := ISat.IsVisible(
                                                SatsDataArray[i].Name,
                                                jt,
                                                MinElev,
                                                SatsDataArray[i].SouthBound,
                                                SatsDataArray[i].az,
                                                SatsDataArray[i].el,
                                                SatsDataArray[i].lon,
                                                SatsDataArray[i].lat,
                                                SatsDataArray[i].range,
                                                SatsDataArray[i].range_rate,
                                                SatsDataArray[i].altitude,
                                                phase);

    SatsDataArray[i].az := SatsDataArray[i].az * Rad2Deg;
    SatsDataArray[i].el := SatsDataArray[i].el * Rad2Deg;
    SatsDataArray[i].lon := SatsDataArray[i].lon * Rad2Deg;
    SatsDataArray[i].lat := SatsDataArray[i].lat * Rad2Deg;

    if SatsDataArray[i].lon > 180 then
      SatsDataArray[i].lon := SatsDataArray[i].lon - 360;

    SatsDataArray[i].poiner.Img := TImage.Create(nil);

    if SatsDataArray[i].Visible then
    begin
      SatsDataArray[i].poiner.Img.Picture.Bitmap := VisSatPointer.Picture.Bitmap;
      if (FirstVis and HDInited ) then
      begin
        TrHDAZ := SatsDataArray[i].az;
        TRHDEL := SatsDataArray[i].el;
        FirstVis := False;
      end;
    end
    else
      SatsDataArray[i].poiner.Img.Picture.Bitmap := NotVisSatPointer.Picture.Bitmap;

    SatsDataArray[i].poiner.Img.OnMouseMove := VisSatPointer.OnMouseMove;

    SatsDataArray[i].poiner.Img.Transparent := true;
    SatsDataArray[i].poiner.Img.Cursor := crCross;
    SatsDataArray[i].poiner.Img.AutoSize := true;
    SatsDataArray[i].poiner.Img.Name := 'N'+IntToStr(i);
    SatsDataArray[i].poiner.Img.Parent := TntTabSheet1;

    SatsDataArray[i].poiner.Lbl := TLabel.Create(nil);
    SatsDataArray[i].poiner.Lbl.OnMouseMove := SatPointerLblMouseMove;
    SatsDataArray[i].poiner.Lbl.Transparent := true;
    SatsDataArray[i].poiner.Lbl.Cursor := crCross;
    SatsDataArray[i].poiner.Lbl.Name := 'L'+IntToStr(i);
    SatsDataArray[i].poiner.Lbl.Parent := TntTabSheet1;

    ShowSat(SatsDataArray[i], TrackSatOrdLst.Items[i]);



    {if SatsDataArray[i].Visible and FirstVis then begin
      FirstVis := false;
      ShowVisSat(SatsDataArray[i], TrackSatOrdLst.Items[i]);
    end;}
  end;

  if FirstVis then begin
    TrHDAZ := 0;
    TRHDEL := 0;
  end;

end;

procedure TMainF.ShowSat(out Sat: SatsDataRec; SatName: string);
var
vis, range_rate, lonp, latp:string;

begin
  Sat.poiner.Img.Left := round( ((WorldMap.Width - Sat.poiner.Img.Width + (Sat.lon * WorldMap.Width) / 180) / 2) ) ;
  Sat.poiner.Img.Top := WorldMap.Height - round( WorldMap.Height / 2 - Sat.poiner.Img.Height / 2
                    + ( (Sat.lat / 180) * (WorldMap.Height) )-WorldMap.Top);
  Sat.poiner.Img.Visible := true;

  Sat.poiner.Lbl.Caption := SatName;
  if (Sat.poiner.Img.Left + Sat.poiner.Lbl.Width) < WorldMap.Width then
    Sat.poiner.Lbl.Left := Sat.poiner.Img.Left
  else
    Sat.poiner.Lbl.Left := Sat.poiner.Img.Left + Sat.poiner.Img.Width - Sat.poiner.Lbl.Width;

  if (Sat.poiner.Img.Top + Sat.poiner.Lbl.Height) < WorldMap.Height then
    Sat.poiner.Lbl.Top := Sat.poiner.Img.Top + Sat.poiner.Lbl.Height
  else
    Sat.poiner.Lbl.Top := Sat.poiner.Img.Top - Sat.poiner.Lbl.Height;
  Sat.poiner.Lbl.Visible := true;

  if sat.range_rate > 0 then
    range_rate := 'leaving'
  else
    range_rate := 'approaching';

  if sat.lon > 0 then
    lonp := 'E'
  else
    lonp := 'W';

  if sat.lat > 0 then
    latp := 'S'
  else
    latp := 'N';

  vis := 'not visible';
  if Sat.Visible then vis := 'visible';

  //foot print
  DrawFootPrint(Sat.range, Sat.lat, Sat.lon);

  Sat.poiner.Hint := Format(SatName+' is '+ vis +#13+#13+
                'Lon: %f�' +lonp+#13+
                'Lat: %f�' +latp+#13+
                'Distance: %f km' +#13+
                'Speed: %f km/s ' +range_rate+#13+
                'Height: %f km'
                ,[abs(sat.lon), abs(sat.lat), sat.range, abs(sat.range_rate), sat.altitude]
                );
end;

procedure TMainF.VisSatPointerMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
LocHint.ShowControlMessage(SatsDataArray[
                                          strtoint(
                                          copy((Sender as Tcontrol).Name,2,length((Sender as Tcontrol).Name)))
                                        ].poiner.Hint
                                        , (Sender as Tcontrol));
HideLocHint.Enabled := true;
end;

procedure TMainF.TrackSatTimerTimer(Sender: TObject);
begin
TrackSats;
TrackSatStatLst.OnChange(TrackSatStatLst);
end;

procedure TMainF.N1seccond1Click(Sender: TObject);
begin
TrackSatTimer.Interval := 1000;
(Sender as TTntMenuItem).Checked := true;
end;

procedure TMainF.N5seccond1Click(Sender: TObject);
begin
TrackSatTimer.Interval := 5000;
(Sender as TTntMenuItem).Checked := true;
end;

procedure TMainF.N10seccond1Click(Sender: TObject);
begin
TrackSatTimer.Interval := 10000;
(Sender as TTntMenuItem).Checked := true;
end;

procedure TMainF.N30seccond1Click(Sender: TObject);
begin
TrackSatTimer.Interval := 30000;
(Sender as TTntMenuItem).Checked := true;
end;

procedure TMainF.N1minute1Click(Sender: TObject);
begin
TrackSatTimer.Interval := 60000;
(Sender as TTntMenuItem).Checked := true;
end;

procedure TMainF.N5minutes1Click(Sender: TObject);
begin
TrackSatTimer.Interval := 300000;
(Sender as TTntMenuItem).Checked := true;
end;

procedure TMainF.TrackSatStatLstChange(Sender: TObject);
begin
  if TrackSatStatLst.ItemIndex = -1 then begin
    LblTrackSatAz.Caption := '0.0';
    LblTrackSatEl.Caption := '0.0';
    LblTrackSatLon.Caption := '0.0';
    LblTrackSatLat.Caption := '0.0';
    LblTrackSatDistance.Caption := '0.0';
    LblTrackSatSpeed.Caption := '0.0';
    LblTrackSatAltitude.Caption := '0.0';
    exit;
  end;

  if SatsDataArray[TrackSatStatLst.ItemIndex].Visible then
    LblTrackSatVis.Caption := 'visible'
  else
    LblTrackSatVis.Caption := 'not visible';

  LblTrackSatAz.Caption := Format('%10.5f', [SatsDataArray[TrackSatStatLst.ItemIndex].az]);
  LblTrackSatEl.Caption := Format('%10.5f', [SatsDataArray[TrackSatStatLst.ItemIndex].el]);
  LblTrackSatLon.Caption := Format('%10.5f', [SatsDataArray[TrackSatStatLst.ItemIndex].lon]);
  LblTrackSatLat.Caption := Format('%10.5f', [SatsDataArray[TrackSatStatLst.ItemIndex].lat]);
  LblTrackSatDistance.Caption := Format('%10.5f', [SatsDataArray[TrackSatStatLst.ItemIndex].range]);
  LblTrackSatSpeed.Caption := Format('%10.5f', [SatsDataArray[TrackSatStatLst.ItemIndex].range_rate]);
  LblTrackSatAltitude.Caption := Format('%10.5f', [SatsDataArray[TrackSatStatLst.ItemIndex].altitude]);
end;

procedure TMainF.MakeVisRange(SatName: string);
var
LonR, LatR : word;
Co : integer;
StateCh : boolean;
b : WordBool;
jt, a : Double;
tmp : Currency;
begin

  Co := 0;
  ISat.ObsLon := 0;
  ISat.ObsLat := -90;
  StateCh := ISat.IsVisible(SatName, ISat.DateTimeToJT(nowUTC), 0, b, a, a, a, a, a, a, a, a);
  jt := ISat.DateTimeToJT(nowUTC);

  for LatR := 0 to 180 do begin
    ISat.ObsLon := -180;
    for LonR := 0 to 360 do begin
      if (StateCh <> ISat.IsVisible(SatName, jt,
                         0, b, a, a, a, a, a, a, a, a)) then begin
        Co := Co + 1;
        SetLength(VisRange, Co);
        VisRange[Co-1].Y := WorldMap.Height - round( (WorldMap.Height / 2) + ( (ISat.ObsLat / 180) * (WorldMap.Height) )-WorldMap.Top);

        if ISat.ObsLon > 180 then
          tmp := ISat.ObsLon - 360
        else
          tmp := ISat.ObsLon;

        VisRange[Co-1].X := round(WorldMap.Width /2 + (tmp * WorldMap.Width) / 360);
        StateCh := ISat.IsVisible(SatName, ISat.DateTimeToJT(nowUTC), 0, b, a, a, a, a, a, a, a, a);
      end;
      ISat.ObsLon := ISat.ObsLon + 1;
    end;
    ISat.ObsLat := ISat.ObsLat + 1;
  end;

{  WorldMap.Canvas.Pen.Style := psSolid;
  WorldMap.Canvas.Pen.Color := clwhite;
  WorldMap.Canvas.Pen.Width := 1;
  WorldMap.Canvas.Polygon(VisRange);}
  for LonR := 0 to Co do
    WorldMap.Canvas.Pixels[VisRange[LonR].X, VisRange[LonR].Y] := clblue;

  ReadLocData;
end;

procedure TMainF.MakeVisRange2(SatName: string);
var
r : Currency;
a, tmp, FLat, TLat, TLon : Double;
i, Co : Word;
b : WordBool;
XObs,YObs: integer;
begin

  Co := 0;
  FLat := ISat.ObsLat;
  while ISat.IsVisible(SatName, ISat.DateTimeToJT(nowUTC), 0, b, a, a, a, a, a, a, a, a) do
    ISat.obslat := ISat.obslat + 0.5;

  r := ISat.ObsLat - FLat;

  XObs := VisSatPointer.Left + VisSatPointer.Width div 2;
  YObs := VisSatPointer.Top + VisSatPointer.Height div 2;

  // x^2 + y^2 = r^2
  TLat := -round(r)-1;
  for i := 0 to 2*round(r) do begin
    TLat := TLat + 1;
    TLon := - round( sqrt( abs(power(r, 2) - power(TLat , 2)) ));

    Co := Co + 1;
    SetLength(VisRange, Co);
    VisRange[Co-1].Y := WorldMap.Height - round( (WorldMap.Height / 2) + ( (TLat / 180) * (WorldMap.Height) )-WorldMap.Top);

    if TLon > 180 then
      tmp := TLon - 360
    else
      tmp := TLon;
    VisRange[Co-1].X := round(WorldMap.Width /2 + (tmp * WorldMap.Width) / 360);
  end;

  TLat := -round(r)-1;
  for i := 0 to 2*round(r) do begin
    TLat := TLat + 1;
    TLon := round( sqrt( abs(power(r, 2) - power(TLat , 2)) ));

    Co := Co + 1;
    SetLength(VisRange, Co);
    VisRange[Co-1].Y := YObs + WorldMap.Height - round( (WorldMap.Height / 2) + ( (TLat / 180) * (WorldMap.Height) )-WorldMap.Top);

    if TLon > 180 then
      tmp := TLon - 360
    else
      tmp := TLon;
    VisRange[Co-1].X := XObs + round(WorldMap.Width /2 + (tmp * WorldMap.Width) / 360);
  end;


  for i := 0 to Co do
    WorldMap.Canvas.Pixels[VisRange[i].X, VisRange[i].Y] := clblue;

  ReadLocData;

end;

procedure TMainF.TntSpeedButton13Click(Sender: TObject);
begin
MainF.Enabled := false;
DownF.Show;
DownF.Refresh;
sleep(100);
try
  DownF.DownUpdates(KDataPathEdt.Text);
finally
  DownF.Hide;
  MainF.Enabled := true;
end;
end;

procedure TMainF.LocPointerLblMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
LocPointerMouseMove(LocPointer, Shift, X, Y);
end;

procedure TMainF.SatPointerLblMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
VisSatPointerMouseMove(SatsDataArray[
                                    strtoint(
                                    copy((Sender as Tcontrol).Name,2,length((Sender as Tcontrol).Name)))
                                  ].poiner.Img
                                  , Shift, X, Y);
end;

function TMainF.ExecAndWait(const ExecuteFile,
  ParamString: string): boolean;
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
begin
  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);
  with SEInfo do begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    Wnd := Application.Handle;
    lpFile := PChar(ExecuteFile);
    lpParameters := PChar(ParamString);
    nShow := SW_HIDE;
  end;
  if ShellExecuteEx(@SEInfo) then
  begin
    repeat
      Application.ProcessMessages;
      GetExitCodeProcess(SEInfo.hProcess, ExitCode);
    until (ExitCode <> STILL_ACTIVE) or Application.Terminated;
    Result:=True;
  end
  else Result:=False;
end;

procedure TMainF.FreeSatPointers(SDA: array of SatsDataRec);
var
i: word;
begin
  if high(SDA) = -1 then exit;
  for i:= low(SDA) to high(SDA) do begin
    if Assigned(SDA[i].poiner.Img) then begin
      SDA[i].poiner.Img.Free;
      SDA[i].poiner.Img := nil;
    end;
    if Assigned(SDA[i].poiner.Lbl) then begin
      SDA[i].poiner.Lbl.Free;
      SDA[i].poiner.Lbl := nil;
    end;
  end;
end;

procedure TMainF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if HDInited then
    ComPort1.Close;

  FreeSatPointers(SatsDataArray);
  RemoveFontResource(pchar(extractfiledir(ParamStr(0)) + '\nrkis.ttf'));
  SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
  SaveUpdateURLs;  
end;

procedure TMainF.KDataDate;
var
  Dir : string;
  fsRec  : TSearchRec;
  fTimeM : TFileTime;
  lTime  : TFileTime;
  sTime  : TSystemTime;
  fDate  : TDateTime;
  lDate  : TDateTime;
  i : Word;
  Dar : array of TDateTime;

begin
//here we define the newest and the oldest date of kepler data
  if KDataPathEdt.Text <> '' then
  begin

  dir := ExcludeTrailingBackslash(KDataPathEdt.Text) + '\';

  if (FindFirst(dir + '*.*', faAnyFile, fsRec) = 0) then
    repeat
      if (fsRec.Name <> '') AND (fsRec.Name <> '.') AND (fsRec.Name <> '..') then
      begin
        if not((fsRec.Attr AND faDirectory) = faDirectory) then
        begin
          GetFileTime(FileOpen(dir + fsRec.Name, fmShareDenyNone), nil, nil, @fTimeM);
          FileTimeToLocalFileTime(fTimeM, lTime);
          if FileTimeToSystemTime(lTime, sTime)
            then fDate := EncodeDate(sTime.wYear, sTime.wMonth, sTime.wDay) + EncodeTime(sTime.wHour, sTime.wMinute, sTime.wSecond, sTime.wMilliSeconds);
          SetLength(Dar, length(Dar)+1);
          Dar[Length(Dar)-1] := fDate;
        end;
      end;
    until (FindNext(fsRec) <> 0);

    if Length(Dar) = 0 then
    begin
      KDDateGroup.Visible := false;
      exit;
    end;

    fDate := Dar[0];
    lDate := Dar[0];
    for i := 0 to length(Dar) -1 do
    begin
      if fDate < Dar[i] then
        fDate := Dar[i];

      if lDate > Dar[i] then
        lDate := Dar[i];
    end;

    KDDateGroup.Visible := true;
    KDNewLbl.Caption := DateTimeToStr(fDate);
    KDOldLbl.Caption := DateTimeToStr(lDate);

  end
  else
  begin
      KDDateGroup.Visible := false;
  end;

end;


procedure TMainF.TntSpeedButton12Click(Sender: TObject);
begin
  UpdateURLEdit.Text := UpdateDownLst.Items[UpdateDownLst.ItemIndex];
  UpdateDownLst.Items.Delete(UpdateDownLst.ItemIndex);
end;

procedure TMainF.TntSpeedButton1Click(Sender: TObject);
begin
  UpdateDownLst.Items.Add(UpdateURLEdit.Text);
  UpdateURLEdit.Text := '';
end;

procedure TMainF.TntSpeedButton5Click(Sender: TObject);
begin
  UpdateDownLst.Items.Delete(UpdateDownLst.ItemIndex);
end;

procedure TMainF.UTCDisplayTimerTimer(Sender: TObject);
begin
  UTCDisplay.Caption := TimeToStr(NowUTC);
  UTCDateDisplay.Caption := DateToStr(NowUTC);
end;

procedure TMainF.KeplerAlertClick(Sender: TObject);
begin
  if KeplerAlert.Checked then
  begin
    KeplerAlertTimer.Enabled := true;
    AlertDays.Enabled := True;
    WriteStrInReg('KeplerAlert', '1');
  end
  else
  begin
    KeplerAlertTimer.Enabled := false;
    AlertDays.Enabled := False;
    WriteStrInReg('KeplerAlert', '0');
  end;

end;

procedure TMainF.KeplerAlertKeyPress(Sender: TObject; var Key: Char);
begin
  if KeplerAlert.Checked then
  begin
    KeplerAlertTimer.Enabled := true;
    AlertDays.Enabled := True;
    WriteStrInReg('KeplerAlert', '1');
  end
  else
  begin
    KeplerAlertTimer.Enabled := false;
    AlertDays.Enabled := False;
    WriteStrInReg('KeplerAlert', '0');
  end;

end;

procedure TMainF.KeplerAlertTimerTimer(Sender: TObject);
var
diff : Extended;
begin

  KDataDate;
  If not KDDateGroup.Visible then exit;
  diff := now - StrToDateTime(KDOldLbl.Caption);
  if (diff / 86400) > StrToInt(AlertDays.Text) then
    ShowMessage('Kepler data is older than '+AlertDays.Text+' days.'+#13+
                'It is highly recommanded to update it now!');

end;

procedure TMainF.SaveUpdateURLs;
begin
  UpdateDownLst.Items.SaveToFile(extractfiledir(ParamStr(0)) + '\UPDURLS.dat');
end;

procedure TMainF.LoadUpdateURLs;
begin
  if FileExists(extractfiledir(ParamStr(0)) + '\UPDURLS.dat') then
    UpdateDownLst.Items.LoadFromFile(extractfiledir(ParamStr(0)) + '\UPDURLS.dat');
end;

procedure TMainF.AlertDaysChange(Sender: TObject);
begin
  WriteStrInReg('KeplerAlertInterval', AlertDays.Text);

end;

procedure TMainF.HDInitClick(Sender: TObject);
var
  RxStr : String;
  Retries : Word;
begin
  if ComPortsCB.Text = '' then
  begin
    ShowMessage('First select a port!');
    Exit;
  end;

  ComPort1.Port := ComPortsCB.Text;

  try

    ComPort1.Open;
    ComPort1.WriteStr(chr(12));

    Retries := 0;
    repeat
      ComPort1.ReadStr(RxStr, 3);
      Inc(Retries);
    until not ( (Length(RxStr) <> 3) and (Retries  < 10) );

    if (Length(RxStr) <> 3) or (RxStr <> 'HDI') then
    begin
      Application.MessageBox('Not a hardware answer or wrong one,'+#13+
                              'Check every thing and try again.', 'Error');
      ComPort1.Close;
      LblHDDetection.Caption := 'Not detected';
    end
    else
    begin
      ComPort1.WriteStr(HDInitHeader);
      HDInited := True;
      DataTransmit.Enabled := True;
      HDInit.Enabled := False;
      ComPortsCB.Enabled := False;
      HDDisConnect.Enabled := True;
      LblHDDetection.Caption := 'Ready';
      NewTipBtn.Click;
      HDLog.Text := 'Hardware initialized on port "' + ComPort1.Port+'"';
      Application.MessageBox('Hardware detected and is ready.', 'Hint');
    end;

  except
    ShowMessage('There was an error opening the selected port,'+#13+
                'select another port and try again.');
    exit;
  end;

end;

procedure TMainF.DrawFootPrint(RS, slat, slon: Double);
var
srad, cla, clo, sra, A, sla, slo, cra,
Xb, Yb, Zb, x, y, z : Double;
i, PX, PY : Word;

const
  Deg2Rad = pi / 180;

begin

  exit;
  // Take satellite distance, sub-satellite lat/lon and compute unit vectors'
  // x,y,z of N points of footprint on Earth's surface in Geocentric
  // Coordinates.  Also terrestrial latitude and longitude of points.

  slat := slat * Deg2Rad;
  slon := slon * Deg2Rad;

  srad := ArcCos(6378.137/RS);         // Radius of footprint circle
  cla := COS(slat); sla := SIN(slat);  // Sin/Cos these to save time
  clo := COS(slon); slo := SIN(slon);
  sra := SIN(srad); cra := COS(srad);

  WorldMap.Canvas.Brush.Color := RGBToColor(255,255,255);;

  for i := 0 to foot_print_points do
  begin

    A := 2*PI*i/foot_print_points;     // Angle around the circle

    Xb := cra;             // Circle of points centred on Lat:=0, Lon:=0
    Yb := sra*SIN(A);      // assuming Earth's radius := 1
    Zb := sra*COS(A);      // [ However, use a table for SIN(.)  COS(.) ]

    x := Xb*cla - Zb*sla;   // Rotate point "up" by latitude  slat
    y := Yb;
    Zb := Xb*sla + Zb*cla;

    Xb := x*clo - y*slo;   // Rotate point "around" through longitude  slon
    Yb := x*slo + y*clo;
    Zb := Zb;

    x := FNatn(Yb,Xb);
    y := ArcSin(Zb);
    
    // Convert point to Lat/Lon
    PX := round( ((WorldMap.Width + (FNatn(Yb,Xb) * WorldMap.Width) / 180) / 2) );
    PY := WorldMap.Height - round( WorldMap.Height / 2 +
          ( (ArcSin(Zb) / 180) * (WorldMap.Height) )-WorldMap.Top);

    if i = 0 then
      WorldMap.Canvas.MoveTo(PX, PY)
    else
      WorldMap.Canvas.LineTo(PX, PY);

  end;

end;

function TMainF.FNatn(x, y: Double): Double;
begin

  IF X <> 0 THEN Result := ArcTan(Y/X) ELSE Result := PI/2*Sign(Y);
  IF X < 0 THEN Result := Result + PI;
  IF Result < 0 THEN Result := Result + 2 * PI;

end;

procedure TMainF.TransmitData(az, el: Double; turn:string);
var
  RxTxStr : String;
  Retries : Word;
begin
//Send az and el to hardware
  if turn = 'az' then
  begin
    //ComPort1.WriteStr(HDAZHeader);
    ComPort1.WriteStr('a');

    Retries := 0;
    repeat
      ComPort1.ReadStr(RxTxStr, 1);
      Inc(Retries);
    until not ( (Length(RxTxStr) <> 1) and (Retries  < 3) );

    if LowerCase(RxTxStr) = 'y' then
    begin
      RxTxStr := 'dps';
      RxTxStr[1] := chr(trunc(az) mod 256); //send lower byte then
      RxTxStr[2] := chr((trunc(az) div 256)*16 + trunc((az - trunc(az)) * 10) ); //send one digit decimal
      RxTxStr[3] := chr(ChkSum(RxTxStr[1], RxTxStr[2]));

      ComPort1.WriteStr(RxTxStr);
    end;
  end
  else
  begin
    //ComPort1.WriteStr(HDAZHeader);
    ComPort1.WriteStr('e');

    Retries := 0;
    repeat
      ComPort1.ReadStr(RxTxStr, 1);
      Inc(Retries);
    until not ( (Length(RxTxStr) <> 1) and (Retries  < 3) );

    if LowerCase(RxTxStr) = 'y' then
    begin
      RxTxStr := 'dps';
      RxTxStr[1] := chr(trunc(el) mod 256); //send lower byte
      RxTxStr[2] := chr( trunc((el - trunc(el)) * 10) ); //send one digit decimal
      RxTxStr[3] := chr(ChkSum(RxTxStr[1], RxTxStr[2]));

      ComPort1.WriteStr(RxTxStr);
    end;
  end;

end;

procedure TMainF.HDDisConnectClick(Sender: TObject);
begin

      HDInited := False;
      DataTransmit.Enabled := False;
      HDInit.Enabled := True;
      ComPortsCB.Enabled := True;
      HDDisConnect.Enabled := False;
      LblHDDetection.Caption := 'Disconnected';
      NewTipBtn.Click;
      ComPort1.Close;
      HDLog.Text := 'Disconnected from hardware.';
      Application.MessageBox('Disconnected from hardware.', 'Hint');

end;

procedure TMainF.DataTransmitTimer(Sender: TObject);
begin

  if (TrTurn = '') or (TrTurn = 'el') then
    TrTurn := 'az'
  else if (TrTurn = 'az') then 
    TrTurn := 'el';

  if (TrHDAZ <> 0) and (TRHDEL <> 0) then
    TransmitData(TrHDAZ, TRHDEL, TrTurn);
end;

function TMainF.ChkSum(a, b: char): Byte;
var
  Res : String;
  Data : String;
begin

  //makeup the check sum of byte
  Res := '00000000';
  Data := IntToBin(ord(a));

  Res[4] := chr(48 + (StrToInt(Data[8]) xor StrToInt(Data[7]) xor StrToInt(Data[5]) xor StrToInt(Data[4]) xor StrToInt(Data[2])) );
  Res[3] := chr(48 + (StrToInt(Data[8]) xor StrToInt(Data[6]) xor StrToInt(Data[5]) xor StrToInt(Data[3]) xor StrToInt(Data[2])) );
  Res[2] := chr(48 + (StrToInt(Data[7]) xor StrToInt(Data[6]) xor StrToInt(Data[5]) xor StrToInt(Data[1])) );
  Res[1] := chr(48 + (StrToInt(Data[5]) xor StrToInt(Data[3]) xor StrToInt(Data[2]) xor StrToInt(Data[1])) );

  Data := '';
  Data := IntToBin(ord(b));

  Res[8] := chr(48 + (StrToInt(Data[8]) xor StrToInt(Data[7]) xor StrToInt(Data[5]) xor StrToInt(Data[4]) xor StrToInt(Data[2])) );
  Res[7] := chr(48 + (StrToInt(Data[8]) xor StrToInt(Data[6]) xor StrToInt(Data[5]) xor StrToInt(Data[3]) xor StrToInt(Data[2])) );
  Res[6] := chr(48 + (StrToInt(Data[7]) xor StrToInt(Data[6]) xor StrToInt(Data[5]) xor StrToInt(Data[1])) );
  Res[5] := chr(48 + (StrToInt(Data[5]) xor StrToInt(Data[3]) xor StrToInt(Data[2]) xor StrToInt(Data[1])) );

  Result := BinToInt(Res);

end;

end.
