program MaxTrack;

uses
  Forms,
  MainFrm in 'MainFrm.pas' {MainF},
  AboutFrm in 'AboutFrm.pas' {AboutF},
  CityFrm in 'CityFrm.pas' {CityF},
  DownFrm in 'DownFrm.pas' {DownF},
  StartUpFrm in 'StartUpFrm.pas' {StartUpF};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Max Track';
  Application.CreateForm(TMainF, MainF);
  Application.CreateForm(TAboutF, AboutF);
  Application.CreateForm(TCityF, CityF);
  Application.CreateForm(TDownF, DownF);
  Application.Run;
end.
