unit StartUpFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LMDCustomComponent, LMDWndProcComponent, LMDFormShape,
  TntStdCtrls, StdCtrls, ExtCtrls;

type
  TStartUpF = class(TForm)
    LMDFormShape1: TLMDFormShape;
    TntLabel1: TTntLabel;
    Bevel1: TBevel;
    TntLabel2: TTntLabel;
    LoadProgLbl: TTntLabel;
    TntLabel3: TTntLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StartUpF: TStartUpF;

implementation

{$R *.dfm}

procedure TStartUpF.FormCreate(Sender: TObject);
begin
  AddFontResource(pchar(extractfiledir(ParamStr(0)) + '\nrkis.ttf'));
  SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0) ;
end;

end.
