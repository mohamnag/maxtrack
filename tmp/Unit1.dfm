object Form1: TForm1
  Left = 411
  Top = 291
  Caption = 'Form1'
  ClientHeight = 392
  ClientWidth = 332
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ComLed1: TComLed
    Left = 64
    Top = 288
    Width = 25
    Height = 25
    ComPort = ComPort1
    LedSignal = lsConn
    Kind = lkRedLight
  end
  object PortsCB: TComboBox
    Left = 32
    Top = 32
    Width = 105
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = PortsCBChange
  end
  object HardInitBtn: TButton
    Left = 96
    Top = 88
    Width = 49
    Height = 25
    Caption = 'Send'
    TabOrder = 1
    OnClick = HardInitBtnClick
  end
  object Edit1: TEdit
    Left = 32
    Top = 88
    Width = 57
    Height = 21
    MaxLength = 8
    TabOrder = 2
  end
  object Button1: TButton
    Left = 32
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
    OnClick = Button1Click
  end
  object ComRadioGroup1: TComRadioGroup
    Left = 120
    Top = 184
    Width = 185
    Height = 105
    ComPort = ComPort1
    ComProperty = cpStopBits
    Caption = 'ComRadioGroup1'
    ItemIndex = 0
    TabOrder = 4
  end
  object ComComboBox1: TComComboBox
    Left = 128
    Top = 312
    Width = 145
    Height = 21
    ComPort = ComPort1
    ComProperty = cpPort
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = -1
    TabOrder = 5
  end
  object ComPort1: TComPort
    BaudRate = br9600
    Port = 'COM1'
    Parity.Bits = prOdd
    StopBits = sbOneStopBit
    DataBits = dbEight
    Events = [evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS, evDSR, evError, evRLSD, evRx80Full]
    FlowControl.OutCTSFlow = False
    FlowControl.OutDSRFlow = False
    FlowControl.ControlDTR = dtrDisable
    FlowControl.ControlRTS = rtsDisable
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    Left = 32
    Top = 56
  end
end
