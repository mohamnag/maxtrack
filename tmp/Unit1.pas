unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,CPort, CPortCtl, ExtCtrls;

type
  TForm1 = class(TForm)
    PortsCB: TComboBox;
    HardInitBtn: TButton;
    ComPort1: TComPort;
    Edit1: TEdit;
    Button1: TButton;
    ComLed1: TComLed;
    ComRadioGroup1: TComRadioGroup;
    ComComboBox1: TComComboBox;
    procedure HardInitBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure PortsCBChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

  PortInited : Boolean;

implementation

uses Unit2;

{$R *.dfm}
function BinToInt(Value: string): Integer;
var
  i, iValueSize: Integer;
begin
  Result := 0;
  iValueSize := Length(Value);
  for i := iValueSize downto 1 do
    if Value[i] = '1' then Result := Result + (1 shl (iValueSize - i));
end; 

procedure TForm1.HardInitBtnClick(Sender: TObject);
var
Operation1: PAsync;
ch : string;

begin
  ComPort1.Port := PortsCB.Text;

{  HardInitFrm.Show;
  Form1.Enabled := false;
}
  ComPort1.Open;

  InitAsync(Operation1);
  try
    ch := Chr( BinToInt(Edit1.Text) );// StrToInt(Edit1.text));
    ComPort1.WriteAsync(Ch[1], 1, Operation1);
    // do some stuff here
    ComPort1.WaitForAsync(Operation1);
  finally
    DoneAsync(Operation1);
  end;

//  Form1.Enabled := true;
//  HardInitFrm.Hide;
  ComPort1.Close;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  EnumComPorts(PortsCB.Items);

  if PortsCB.Items.Count = -1 then
  begin
    ShowMessage('There was an error getting the list of ports.'+#13+
                'In this state program cann''t use hardware.');
    exit;
  end;

  PortsCB.ItemIndex := 0;
  PortInited := true;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  ShowMessage(IntToStr(BinToInt(Edit1.Text)));

end;

procedure TForm1.PortsCBChange(Sender: TObject);
begin
  ComPort1.Port := PortsCB.Text;
end;

end.
